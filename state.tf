terraform {
  backend "s3" {
    bucket = "base-config-340924"
    key    = "gitlab-runner-fleet"
    region = "us-east-1"
  }
}
